build:
	gcc -Wall -g dct.c -lm -o dct

run_dct:
	./dct input.ppm

run_valgrind:
	valgrind --tool=callgrind ./dct input.ppm

clean:
	rm dct out.txt out.ppm
	